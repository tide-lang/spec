.. tide-lang-spec documentation master file, created by
   sphinx-quickstart on Sun Jul  2 08:45:26 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Tide!
================

Tide is a concurrent stream-based programming language, where the stream is both
the base datatype **and** the main method of function execution and coordination.

Tide takes inspiration from languages such as Haskell, Python, Javascript, and Bash.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   language_reference
   standard_library


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
