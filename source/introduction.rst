
Introduction
============

Tide is a concurrent stream-based programming language where the stream is both
the base datatype **and** the main method of function execution and coordination.
Function execution in Tide is structured into the use of pipelines. Take for example
the code below::

  file(name="test.txt") | replace(old="a", new="b") | stdout

Which reads in a file named "test.txt", replaces the string "a" with the string "b",
and prints it to the stdout stream. In this example, all of the functional blocks
(``file``, ``replace``, and ``stdout``) are executing concurrently; each operates
on data as it is received and passes it to the next block through a pipe.

As is tradition, the "Hello World" program in tide looks like so::

  stream "Hello World" | stdout

Philosophy
----------

The core philosophy of the Tide language is that computation is just the transformation
of data, and a language should make the expression of those transformations as simple
as possible.


Features
--------

- Execution model is concurrent-by-default
- Synchronization is handled *implicitly* through I/O
- Visual and textual representation of code
- Function execution is general enough to be distributed across devices
- Abstract pipes allow for different execution models (eg. thread-based, process-based)

Goals
-----

1. Provide a simple way to express data transformations
2. Abstract away concurrent execution of purely functional blocks
3. Abstract away function coordination through piping
