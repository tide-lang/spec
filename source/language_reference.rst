
Language Reference
==================

Streams
-------

The array-bracket ``[]`` notation is used to create wholely complete streams - something
akin to singlely iterable lists in other languages. The stream is populated with the
values provided between the brackets, and is immediately closed.


Functions
---------

Functions can be parameterized (with optional default parameter values),
when calling the function, parameters *must* be named. There are no positional, unnamed
parameters in Tide.


Closures
--------
Curly braces ``{}`` define a closure, and encapsulate a series of expressions.


Pipelines
---------

Pipelines are themselves a stream. The interpreter reads the pipeline stream and starts
each function separately. In this way, all functions are executed concurrently, where the
only synchronization and ordering is performed implicitly through blocking pipe I/O.

Definitions
-----------

A definition in Tide is an immutable binding of an identifier to a fixed value.::

   def key value;

This defines the identifier ``key`` to the value ``value`` permanently. This extends
to function definitions as well - consider the following::

  def ones function(length) {
    for x in range(length=length) {
      yield 1;
    }
  }

This defines a function ``ones`` which 


Visual Language
---------------

Tide is broken up into two different syntax representations: a visual language and a
textual language, colloquially "High Tide" and "Low Tide". High Tide provides a much
higher level view of the program and its consituent components and is rendered into
Low Tide before compilation.

Basic Example: Character Replacement
####################################

As a trivial example, here's a program which replaces all occurrences of the letter
"a" with the letter "b".

.. code-block:: tide
   
               +------+             +---------+   +-------+
  ------------>| file |------------>| replace |-->| print |
  name="x.txt" +------+  old = "a"  +---------+   +-------+
                         new = "b"

Which will compile down to the very simple program::
  read(name="x.txt") | replace(old="a",new="b") | print

In order to make this more interesting, let's include an inline definition for the
``replace`` function.

.. code-block:: tide
  
                                   +------------------------------+
                                   | function replace(old, new) { |
                                   |   for c in input {           |
                                   |     if c == old {            |
               +------+            |       yield new;             |  +-------+
     --------->| read |----------->|     }                        |->| print |
  name="x.txt" +------+ old = "a"  |     else {                   |  +-------+
                        new = "b"  |       yield c;               |
                                   |     }                        |
                                   |   }                          |
                                   | }                            |
                                   +------------------------------+
  

Which will compile down to::

  read(name="x.txt") |
    ( (function replace(old, new) {
        for c in input {
          if c == old {
            yield new;
          }
          else {
            yield c;
          }
        }
      })(old="a",new="b")
    ) |
    print

This is noticeably more complex than the previous case: inlining functions into a
pipeline quickly becomes messy. In order to alleviate that, function definitions
can exist outside of the pipeline like so::

             function
   +------------------------------+
   | function replace(old, new) { |
   |   for c in input {           |
   |     if c == old {            |
   |       yield new;             |
   |     }                        |
   |     else {                   |
   |       yield c;               |
   |     }                        |
   |   }                          |
   | }                            |
   +------------------------------+
  
                +------+             +---------+   +-------+
   ------------>| file |------------>| replace |-->| print |
   name="x.txt" +------+  old = "a"  +---------+   +-------+
                          new = "b"


Which then compiles down to::

  def function replace(old, new) {
    for c in input {
      if c == old {
        yield new;
      }
      else {
        yield c;
      }
    }
  }
  
  read(name="x.txt") | replace(old="a",new="b") | print


Forked Output Pipes
###################

Output pipes in Tide can be forked to multiple sinks; each sink will receive
as input every object that the source outputs (eg. they are duplicated).::
            
  +-----------------+     +-------------+
  | file "test.txt" |--+->| print input |
  +-----------------+  |  +-------------+
                       |
                       |                   +--------+      +-------------+
                       +------------------>| concat |----->| print input |
                         prefix = "pre"    +--------+      +-------------+
                         suffix = "post"


Translates to:::

  file "test.txt" |- [
    print input,
    ( concat(prefix="pre",suffix="post") | print )
  ]
